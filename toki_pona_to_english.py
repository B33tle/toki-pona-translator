# Prints english translations / definitions for words in Toki Pona
from words import terms

while 1:
    inp = input(">").lower().strip()
    try:
        print(terms[inp])
    except:
        print(f"No known definition for {inp}\n")
        
