from words import terms as words
def english_to_toki_pona(d : dict[str, str], desired):
    # if multiple words appear to match, print them all
    possible : list = []

    for key, value in d.items():
        # if what the user entered is an exact definition of a word, or close to one, add it to possible
        if (desired == value) or (desired in value):
            possible.append(key)
    
    #return words that appear to be what the user wanted
    return possible


while 1:
    inp = input(">").lower() #user input


    tp : list = english_to_toki_pona(d = words, desired = inp)
    if len(tp):
        print("Possible English to Toki Pona Traslations : ")
        for elem in tp:
            print(f"{inp} -> {elem}")

    else:
    	print(f"{inp} unable to be translated")
